﻿using System;

namespace _02
{
    public class Intcode
    {
        public static int Operator(int[] arr)
        {
            int count = 0;
            int i1 = 0;
            int i2 = 0;

            for (int i = 0; i <= 5; i+4)
            {
                if (arr[0] == 1) 
                {
                    i1 = arr[1];
                    i2 = arr[2];
                    return arr[i1] + arr[i2];
                }
                if (arr[0] == 2) 
                {
                    i1 = arr[1];
                    i2 = arr[2];
                    return arr[i1] * arr[i2];
                }
                if (arr[0] == 99)
                {
                    break;
                }
                return 0000;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var opcodes = new[]
            {
                1,
                0,
                0,
                3,
                1,
                1,
                2,
                3,
                1,
                3,
                4,
                3,
                1,
                5,
                0,
                3,
                2,
                13,
                1,
                19,
                1,
                10,
                19,
                23,
                1,
                23,
                9,
                27,
                1,
                5,
                27,
                31,
                2,
                31,
                13,
                35,
                1,
                35,
                5,
                39,
                1,
                39,
                5,
                43,
                2,
                13,
                43,
                47,
                2,
                47,
                10,
                51,
                1,
                51,
                6,
                55,
                2,
                55,
                9,
                59,
                1,
                59,
                5,
                63,
                1,
                63,
                13,
                67,
                2,
                67,
                6,
                71,
                1,
                71,
                5,
                75,
                1,
                75,
                5,
                79,
                1,
                79,
                9,
                83,
                1,
                10,
                83,
                87,
                1,
                87,
                10,
                91,
                1,
                91,
                9,
                95,
                1,
                10,
                95,
                99,
                1,
                10,
                99,
                103,
                2,
                103,
                10,
                107,
                1,
                107,
                9,
                111,
                2,
                6,
                111,
                115,
                1,
                5,
                115,
                119,
                2,
                119,
                13,
                123,
                1,
                6,
                123,
                127,
                2,
                9,
                127,
                131,
                1,
                131,
                5,
                135,
                1,
                135,
                13,
                139,
                1,
                139,
                10,
                143,
                1,
                2,
                143,
                147,
                1,
                147,
                10,
                0,
                99,
                2,
                0,
                14,
                0
            };
            // opexample1 becomes: 3500,9,10,70,2,3,11,0,99,30,40,50
            var opexample1 = new[]
            {
                1,9,10,3,2,3,11,0,99,30,40,50
            };
            // opexample2 becomes: 2,0,0,0,99 (1 + 1 = 2)
            var opexample2 = new[]
            {
                1,0,0,0,99
            };
            // opexample3 becomes: 2,3,0,6,99 (3 * 2 = 6)
            var opexample3 = new[]
            {
                2,3,0,3,99
            };
            // opexample4 becomes: 2,4,4,5,99,9801 (99 * 99 = 9801)
            var opexample4 = new[]
            {
                2,4,4,5,99,0
            };
            // opexample5 becomes: 30,1,1,4,2,5,6,0,99
            var opexample5 = new[]
            {
                1,1,1,4,99,5,6,0,99
            };

            var opex1 = Intcode.Operator(opexample2);
            var opex2 = Intcode.Operator(opexample3);
            Console.WriteLine(opex1);
            Console.WriteLine(opex2);
        }
    }
}