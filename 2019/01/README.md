--- Day 1: The Tyranny of the Rocket Equation ---

Santa has become stranded at the edge of the Solar System while delivering presents to other planets! To accurately calculate his position in space, safely align his warp drive, and return to Earth in time to save Christmas, he needs you to bring him measurements from fifty stars.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

The Elves quickly load you into a spacecraft and prepare to launch.

At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper. They haven't determined the amount of fuel required yet.

Fuel required to launch a given module is based on its mass. Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.

For example:

    For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
    For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
    For a mass of 1969, the fuel required is 654.
    For a mass of 100756, the fuel required is 33583.

The Fuel Counter-Upper needs to know the total fuel requirement. To find it, individually calculate the fuel needed for the mass of each module (your puzzle input), then add together all the fuel values.

What is the sum of the fuel requirements for all of the modules on your spacecraft?


PUZZLE INPUT

98435
61869
123694
51157
70664
107632
60289
140493
135101
89758
142578
63389
126315
133069
139989
121089
148127
117475
65718
129198
98612
67678
79752
97930
141835
80575
125798
114298
139861
75550
64724
76315
71871
132949
117877
57157
93756
113889
60388
145810
86668
94498
50502
106789
119505
65341
60103
75963
104149
134483
92833
102273
56988
74202
69016
110217
132242
87186
95704
88433
56225
60206
70508
62692
143847
70088
129908
133319
104284
108627
106977
107696
59576
76422
115945
137414
83299
138678
108034
140276
74857
143726
116028
101970
84298
133544
116069
77564
91964
57954
121404
54416
83370
74842
91677
65323
82036
138725
95805
112490
